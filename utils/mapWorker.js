let instanse = null;
let markLayer = null;
export default () => {
  instanse = instanse || L.map('map');
  markLayer = markLayer || L.layerGroup().addTo(instanse);
  return {
    map: instanse,
    markLayer,
  };
}