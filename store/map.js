export const state = () => {
  return {
    leaflet: null,
    tileLayer: null,
    markers: [],
  }
}
export const mutations = {
  setMap( state, { leaflet } ) {
    state.leaflet = leaflet;
  },
  setTileLayer( state, { tileLayer } ) {
    state.tileLayer = tileLayer;
  }
}
export const actions = {
  init( { commit, state } ) {
    const leaflet = new L.Map('map'); // L - leaflet
    commit( 'setMap', { leaflet } );
    // (state.leaflet)
    
    const tileLayer = L.tileLayer(
      'https://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png',
      {
        maxZoom: 18,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, &copy; <a href="https://carto.com/attribution">CARTO</a>',
      }
    );

    commit( 'setTileLayer', { tileLayer } ); 
    
    // state.leaflet.map('map').setView([38.63, -90.23], 13)
    
    state.tileLayer.addTo(state.leaflet);
    

  },
  createMarker( { commit }, { lat, lng, title } ) {
    
    // commit('setMap', 'map')
    // if( !lat || !lng ||  ) return;
  }
}