FROM node:12.13.1-alpine
ARG version=production
WORKDIR /app
COPY . .
RUN yarn install
RUN yarn build
EXPOSE 8000
CMD ["yarn", "start"]